// Função delay aciona o .then após 1s

const delay = () => new Promise(resolve => setTimeout(resolve, 1000));


const umPorSegundo = async () => {
    await delay();
    console.log('1s');

    await delay();
    console.log('1s');

    await delay();
    console.log('1s');
}

umPorSegundo();


//item b

import axios from 'axios';

const getUserFromGithub = async user => {
    try {
        const response = await axios.get(`https://api.github.com/users/${user}`);
        console.log(response.data);
    }
    catch (erro) {
        console.warn('Usuário não existe');
    }

}
getUserFromGithub('diego3g');
getUserFromGithub('diego3g124123');



//item c

class Github {

    static async getRepositories(repo) {
        try {
            const response = await axios.get(`https://api.github.com/repos/${repo}`);
            console.log(response.data);
        }

        catch (erro) {
            console.log('Repositório não existe');
        }
    }
}

Github.getRepositories('rocketseat/rocketseat.com.br');
Github.getRepositories('rocketseat/dslkvmskv');



//item d



const buscaUsuario = async usuario => {
    try {
        const response = await axios.get(`https://api.github.com/users/${usuario}`);
        console.log(response.data);
    }
    catch (erro) {
        console.warn('Usuário não existe');
    }
}
buscaUsuario('diego3g');








